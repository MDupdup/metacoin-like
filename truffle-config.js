const HDWalletProvider = require("truffle-hdwallet-provider");
require("dotenv").config();

module.exports = {

  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    test: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    ropsten: {
        provider: () => {
            return new HDWalletProvider(process.env.MNEMONIC, "https://ropsten.infura.io/v3/" + process.env.INFURA_KEY);
        },
        host: "127.0.0.1",
        port: 7545,
        network_id: "3"
    }
  }
};
