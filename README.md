# MetaCoin

Le projet contient un fichier `.env` en local, qui charge les variables d'environnement *INFURA_KEY* et *MNEMONIC* (le mnemonic de mon compte Metamask).

La monnaie s'appelle le **MetaCoin** (MTC), l'utilisateur reçoit 666 unités de la monnaie.

La monnaie est présente sur le réseau **Ropsten** en local.

## Coordonnées

Nom / Prénom : DUPONT Malo

adresse mail : malo.dupont@ynov.com